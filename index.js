/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function promptDetails(){
		let fullName = prompt("Enter your full name:");
		let age = prompt("Enter your age:");
		let location = prompt("Enter your current location:")

		console.log("Hi " + fullName);
		console.log(fullName + "'s age is " + age);
		console.log(fullName + " is located at " +location);
	};

	promptDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteBands(){
		let faveBands = ["BTS", "Paramore", "Twice", "88Rising", "Dan & Shay"]
		console.log(faveBands);

		// console.log("BTS");
		// console.log("Paramore");
		// console.log("Twice");
		// console.log("88Rising");
		// console.log("Dan & Shay");
	};

	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes or IMDB rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayMovies(){
		console.log("1. The Avengers: Endgame");
		console.log("Tomatometer for Endgame: 94%");
		console.log("2. The Avengers: Infinity War");
		console.log("Tomatometer for Infinity War: 85%");
		console.log("3. The Girl Who Leapt Through Time");
		console.log("Tomatometer for The Girl Who Leapt Through Time: 83%");
		console.log("4. Kimi No Nawa");
		console.log("Tomatometer for Kimi No Nawa: 98%");
		console.log("5. Hello World");
		console.log("Audience Score for Hello World : 82%");

	}
	displayMovies();
/*

	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);
